import java.util.Scanner

val NOT_ODD_LIST_EXCEPTION = IllegalArgumentException("The number of elements must be odd")
val NOT_MAGIC_LIST_EXCEPTION = IllegalArgumentException("It was not a magic list")

fun main() {
    val reader = Scanner(System.`in`)
    print("Enter a number: ")
    val count = reader.nextInt()

    val list = generateMagicList(count)

    print("magic list: $list\n")
    print("uniq number: ${findUniq(list)}\n")
}

fun findUniq(list: List<Int>): Int {
    val cacheList = mutableListOf<Int>()
    if (list.size % 2 == 0)
        throw  NOT_ODD_LIST_EXCEPTION
    for (n in list) {
        if (cacheList.contains(n))
            cacheList.remove(n)
        else
            cacheList.add(n)
    }
    if (cacheList.size != 1)
        throw NOT_MAGIC_LIST_EXCEPTION
    return cacheList.first()
}

fun generateMagicList(count: Int): MutableList<Int> {
    if (count % 2 == 0)
        throw NOT_ODD_LIST_EXCEPTION
    val halfArray = IntArray(((count + 1) / 2)) { it }.toTypedArray()
    return mutableListOf(*halfArray, *halfArray).apply { removeLast() }
}
