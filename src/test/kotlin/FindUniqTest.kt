import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class FindUniqTest {
    @Test
    fun ifAllowedListThenRightAnswer() {
        val num = findUniq(listOf(1, 2, 1, 2, 3))
        assertEquals(num, 3)
    }

    @Test
    fun errorWhenEvenListSize() {
        assertFailsWith<IllegalArgumentException>(
            message = NOT_ODD_LIST_EXCEPTION.message,
            block = {
                findUniq(listOf(1, 2, 1, 2))
            })
    }

    @Test
    fun errorWhenGivenEmptyList() {
        assertFailsWith<IllegalArgumentException>(
            message = NOT_MAGIC_LIST_EXCEPTION.message,
            block = {
                findUniq(listOf())
            })
    }

    @Test
    fun errorWhenListOddAndMoreThanOneUniqNumber() {
        assertFailsWith<IllegalArgumentException>(
            message = NOT_MAGIC_LIST_EXCEPTION.message,
            block = {
                findUniq(listOf(1, 2, 3))
            })
    }

    @Test // not taken into account in the task
    fun rightWhenOddCountOfEqualValues() {
        val num = findUniq(listOf(1, 2, 1, 2, 2))
        assertEquals(num, 2)
    }

    @Test // not taken into account in the task
    fun errorWhenEvenCountOfEqualValues() {
        assertFailsWith<IllegalArgumentException>(
            message = NOT_ODD_LIST_EXCEPTION.message,
            block = {
                findUniq(listOf(1, 2, 1, 2, 2, 2))
            })
    }
}